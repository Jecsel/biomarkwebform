
module.exports = {
    '@tags': ['demo'],
    'Launching Doctors Portal' : function (client) {
      client
        .url('http://localhost:3000')
        .waitForElementVisible('body', 2000)
        .useXpath()
        .setValue('//*[@id="login_form"]/div/div/div[2]/div/form/div[1]/input', 'BMDoctor0001@gmail.com')
        .setValue('//*[@id="login_form"]/div/div/div[2]/div/form/div[2]/input', '1qw21qw2')
        .execute( function(){
            responsiveVoice.setDefaultVoice("UK English Female");
            responsiveVoice.speak("Hello everyone. I am Alexa. This is my first demo for doctors portal.");
        })
        .pause(7000)
        .click('//*[@id="login_form"]/div/div/div[2]/div/form/div[4]/button')
        .pause(3000)
        .execute( function(){
            responsiveVoice.speak("I will show you different chart types for sprint 22.");
        })
        .pause(5000)
        .click('/html/body/div/div/patient-results/div/div/div/div/div[2]/ul/li[3]')
        .execute( function (){
            responsiveVoice.speak("Let's click Natashah Romanoff.");
        })
        .pause(3000)
        .click("/html/body/div/div/patient-results/all-patient/div/div/div/div/div[2]/table/tbody/tr[2]/td[6]/button[1]")
        .pause(2000)
        .execute( function (){
            responsiveVoice.speak("Currently we have two tabs to view those different chart types. First is Latest and Other one is Summary");
        })
        .pause(5500)
        .click('/html/body/div/div/div/div[2]/div/patient-result-tab/div[2]/ul/li[2]')
        .pause(1000)
        .click('/html/body/div/div/div/div[2]/div/patient-result-tab/div[2]/ul/li[1]')
        .pause(2000)
        .execute( function (){
            responsiveVoice.speak("Under abnormal biomarker's panel. You can click each biomarkers to show the trending data. Let's click Total Cholesterol");
        })
        .pause(8000)
        
        .click('/html/body/div/div/div/div[2]/div/patient-latest-result/div/div/div[1]/div/div/div[2]/div[1]')
        .execute( function (){
            responsiveVoice.speak("Total Cholesterol is type 3 biomarker.");
        })
        .pause(5000)
        .click('/html/body/div/div/div/div[2]/div/div[1]/button')
        
        .pause(1000)
        .click('/html/body/div/div/div/div[2]/div/patient-latest-result/div/div/div[1]/div/div/div[2]/div[2]')
        .execute( function (){
            responsiveVoice.speak("HDL Cholesterol is type 2 biomarker.");
        })
        .pause(5000)
        .click('/html/body/div/div/div/div[2]/div/div[1]/button')

        .pause(1000)
        .click('/html/body/div/div/div/div[2]/div/patient-latest-result/div/div/div[1]/div/div/div[2]/div[7]')
        .execute( function (){
            responsiveVoice.speak("HBA1C is type 4B biomarker.");
        })
        .pause(5000)
        .click('/html/body/div/div/div/div[2]/div/div[1]/button')

        .pause(1000)
        .click('/html/body/div/div/div/div[2]/div/patient-latest-result/div/div/div[1]/div/div/div[2]/div[9]')
        .execute( function (){
            responsiveVoice.speak("Glucose is type 1B biomarker.");
        })
        .pause(10000)
        .end();
    }

    // '@tags': ['demo'],
    // 'Login' : function (client) {
    //     client
    //         .useXpath()
    //         .setValue('//*[@id="login_form"]/div/div/div[2]/div/form/div[1]/input', 'BMDoctor0001@gmail.com')
    //         .pause(5000)
    //         .end();
    //   }
};

