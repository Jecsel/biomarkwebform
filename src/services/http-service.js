( function(){
	"use strict";


	angular
		.module("BiomarkWebForm")
		.service("Http",Http);

		Http.$inject=["$http","BiomarkConfig", "$sessionStorage","$state","$q","$localStorage"];

		function Http( $http, BiomarkConfig, $sessionStorage ,$state , $q, $localStorage){
			// var host = "https://internal-service.biomarking.com/";
			// var host = "/";

			var that = this;
			
			this.app_version = "v1.34.1";

			var url = BiomarkConfig.host;
			Object.toparams = function ObjecttoParams(obj) {
				var p = [];
				for (var key in obj) {
					p.push(key + '=' + encodeURIComponent(obj[key]));
				}
				return p.join('&');
			};

            this.set_token = function(token){
				$sessionStorage.access_token = token;
			}
			this.post = function(path,params){
				$http.defaults.headers.common['x-biomark-token'] = $sessionStorage.access_token  || 0;
				return $http.post(url+path+".json",params);
			}
			this.patch = function(path,params){
				$http.defaults.headers.common['x-biomark-token'] = $sessionStorage.access_token  || 0;
				return $http.patch(url+path+".json",params);
			}
			this.delete = function(path){
				$http.defaults.headers.common['x-biomark-token'] = $sessionStorage.access_token  || 0;
				return $http.delete(url+path+".json");
			}
			this.get = function(path){
				$http.defaults.headers.common['x-biomark-token'] = $sessionStorage.access_token  || 0;
				return $http.get(url+path+".json");
			}
			
		}

})();