( function(){
	"use strict";

	angular
		.module("BiomarkWebForm")
		.constant("MobileConfig",{
			countries:[
				{ "id":1,"name": "Malaysia", "dial_code": "+60", "code": "MY" },
				{ "id":2,"name": "Indonesia", "dial_code": "+62", "code": "ID" },
				{ "id":3,"name": "Philippines", "dial_code": "+63", "code": "PH" },
				{ "id":4,"name": "Singapore", "dial_code": "+65", "code": "SG" },
				
			],
		})
})();
