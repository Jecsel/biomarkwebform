

( function(){

	"use strict";

	angular
		.module('BiomarkWebForm')
        .directive('numberOnlyTemp', 
        
        function(){
        return {
            require: 'ngModel',
            link: function (scope, element, attr, ngModelCtrl) {
                function fromUser(text) {
                    if (text) {
                        var transformedInput = text.replace(/[^0-9\.]/g, '');
                        // if(transformedInput.length > 1 && transformedInput > 45 || transformedInput.length > 1 && transformedInput < 35){
                        //     transformedInput = transformedInput.slice(0, -1)
                        // }
                        if (transformedInput !== text) {
                            ngModelCtrl.$setViewValue(transformedInput);
                            ngModelCtrl.$render();
                        }
                        return transformedInput;
                    }
                    return undefined;
                }            
                ngModelCtrl.$parsers.push(fromUser);
            }
        };
    })
})();
