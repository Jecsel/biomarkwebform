( function(){

	"use strict";

	angular
		.module("BiomarkWebForm")
		.controller('biomarkDateController',biomarkDateController);

		biomarkDateController.$inject = ["DateDropdown"];

		function biomarkDateController( DateDropdown ){
			var vm = this;
			
			vm.dateValid = false;
			DateDropdown.init( function(_data){
				vm.default = {month:0, day:1, year:1900};
				vm.months = DateDropdown.months;
				vm.date = vm.default;

				//reassuring that the default already set
				vm.$onInit = function(){
					if(angular.isDefined(vm.date)){
						vm.date = moment(vm.date).format('YYYY-MM-DD');
						var _date = new Date(vm.date) //.toISOString();

						vm.default.month 	= _date.getMonth();
						vm.default.day 		= _date.getDate();
						vm.default.year 	= _date.getFullYear();
					}
					vm.check_validity();
				}
			});
			
			vm.is_visible = false;
			
			vm.onValueChanged = function( month ){
				vm.default.month = month;
				vm.check_validity();
			}
			
			vm.selectMonth = function(){
				vm.is_visible = !vm.is_visible;
			}
			vm.keyPress = function($event){
				vm.check_validity()
				if(isNaN(vm.default.month)) vm.default.month = -1;
				var keyCode = $event.which || $event.keyCode;

				if (keyCode === 13 || keyCode === 32) {
					$event.preventDefault();
					vm.selectMonth();
				}else if(keyCode === 38){
					$event.preventDefault();
					vm.default.month++;
					if(vm.default.month == 12){
						vm.default.month = 0;
					}else if(vm.default.month < 0){
						vm.default.month = 11;
					}

				}else if(keyCode === 40){
					$event.preventDefault();
					vm.default.month--;
					if(vm.default.month == 12){
						vm.default.month = 0;
					}else if(vm.default.month < 0){
						vm.default.month = 11;
					}

				}
			}
			vm.select = function($event,month){
				vm.check_validity();
				var keyCode = $event.which || $event.keyCode;
				if (keyCode === 13 || keyCode === 32) {
					vm.onValueChanged(month);
				}
			}
			vm.check_validity = function(){
				if(vm.default.month != -1 && vm.default.year && vm.default.day) vm.dateValid = true;
				vm.date = vm.default;
			}
		}
})();