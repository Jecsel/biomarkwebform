( function(){

	"use strict";

	angular
		.module("BiomarkWebForm")
		.component('biomarkDate',{
			bindings:{
				date:"=",
				invalid:"=",
				dateValid: "="
			},
			controller:"biomarkDateController",
			templateUrl:"/components/date/view.html"
		})
})();