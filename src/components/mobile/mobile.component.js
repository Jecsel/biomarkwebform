( function(){

	"use strict";

	angular
		.module("BiomarkWebForm")
		.component('mobileCountry',{
			bindings:{
				country:"=",
				mobile:"=",
				invalid:"=",
				submitted:"="
			},
			controller:"mobileController",
			templateUrl:"/components/mobile/view.html"
		})
})();