( function(){
	
	"use strict";


	angular
		.module("BiomarkWebForm")
		.controller("mobileController",mobileController);

		mobileController.$inject=["MobileConfig"];

		function mobileController( MobileConfig ){
			var vm = this;
			vm.dial_code = "+60"
			vm.code= "MY";
			vm.invalid = true;
			vm.$onInit = function(){
		 		vm.is_visible = false;
				var opt = vm.country || 0;
				vm.countries = MobileConfig.countries;	
				vm.default  = MobileConfig.countries[0];
				
				// vm.mobile_placeholder = "2 1234 5678";
				// vm.mobile_regex = "^[0-9]{1,9}$";
				// vm.mobile_max = 9;
				vm.select_dialcode = function(){
					vm.is_visible = !vm.is_visible;
				}

				if(vm.default.code == undefined){
					vm.default.code = "MY"
					vm.check_data(vm.default.code);
				}else{
					if(vm.country != undefined || vm.country != ""){
						vm.check_data(vm.country);
					}
				}
			}

			vm.closeDropDown = function(){
				vm.is_visible = false;
			}

			vm.check_data = function(data){
				switch(data){
					case "PH":
						vm.dial_code = "+63";
						vm.code = "PH"
						vm.mobile_placeholder = "12 1234 5678";
						vm.mobile_regex = "(^[0-9]{1,9})|(^[0-9]{1,10})";
						vm.mobile_min = 0;
						vm.mobile_max = 10;
					break;
					case "SG":
						vm.dial_code = "+65"
						vm.code = "SG"
						vm.mobile_placeholder = "1234 4567";
						vm.mobile_regex = "^[0-9]{1,8}$";
						vm.mobile_min = 8;
						vm.mobile_max = 8;

					break;
					case "MY":
						vm.dial_code = "+60"
						vm.code = "MY"
						vm.mobile_placeholder = "12 1234 5678";
						vm.mobile_regex = "(^[0-9]{1,9})|(^[0-9]{1,10})";
						vm.mobile_min = 8;
						vm.mobile_max = 10;
					break;
					case "ID":
						vm.dial_code = "+62"
						vm.code = "ID"
						vm.mobile_placeholder = "12 1234 5678";
						vm.mobile_regex = "(^[0-9]{1,9})|(^[0-9]{1,10})";
						vm.mobile_min = 8;
						vm.mobile_max = 12;
					break;
				}
			}

			vm.onValueChanged = function( data ){
				vm.default = data;
				vm.country = data.code;
				vm.check_data(data.code);
			}

			
			vm.mobile_change = function(phone){
				phone == undefined || phone == '' ? vm.invalid = true : vm.invalid = false;
			}
			

		}
})();