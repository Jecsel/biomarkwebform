(function(){
    "use strict";

    angular
        .module("BiomarkWebForm")
        .component("withdrawPopup",{
            bindings:{
				packet:"="
			},
            controller:"withdrawPopupController",
            controllerAs:"vm",
            templateUrl:"/components/popup/withdraw/view.html"
        })
})();