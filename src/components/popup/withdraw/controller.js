(function(){
    "use strict";

    angular
        .module("BiomarkWebForm")
        .controller("withdrawPopupController",withdrawPopupController);

        withdrawPopupController.$inject = ["$state"];

        function withdrawPopupController($state){
            
            var vm = this;
            
            vm.close = function(){
                $state.reload();
                vm.packet = false;
            }
        }
})();