(function(){
    "use strict";

    angular
        .module("BiomarkWebForm")
        .component("warningPopup",{
            bindings:{
                packet:"=",
                data: "=",
                color: "="
            },
            controller:"warningController",
            controllerAs:"vm",
            templateUrl:"/components/popup/warning/view.html"
        })
})();