(function(){
    "use strict";

    angular
        .module("BiomarkWebForm")
        .controller("warningController",warningController);

        warningController.$inject = ["$state", "RiskService"];

        function warningController($state, RiskService){
            
            var vm = this;
            vm.data = {};
            
            vm.close = function(){
                vm.packet = false;
                $state.reload();
            }
        }
})();