(function(){
    "use strict";

    angular
        .module("BiomarkWebForm")
        .controller("successPopupController",successPopupController);

        successPopupController.$inject = ["$state"];

        function successPopupController($state){
            
            var vm = this;

            vm.$onInit = function(){
                vm.date = new Date(vm.risk.form_content[0].widget_content.time).toISOString();
                
            }

            vm.close = function(){
                vm.packet = false;
                $state.reload();
            }
        }
})();