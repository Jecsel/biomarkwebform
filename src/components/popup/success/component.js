(function(){
    "use strict";

    angular
        .module("BiomarkWebForm")
        .component("successPopup",{
            bindings:{
                packet:"=",
                risk: "="
			},
            controller:"successPopupController",
            controllerAs:"vm",
            templateUrl:"/components/popup/success/view.html"
        })
})();