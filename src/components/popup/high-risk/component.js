(function(){
    "use strict";

    angular
        .module("BiomarkWebForm")
        .component("highRiskPopup",{
            bindings:{
				packet:"="
			},
            controller:"highRiskPopupController",
            controllerAs:"vm",
            templateUrl:"/components/popup/high-risk/view.html"
        })
})();