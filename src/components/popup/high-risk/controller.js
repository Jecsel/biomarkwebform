(function(){
    "use strict";

    angular
        .module("BiomarkWebForm")
        .controller("highRiskPopupController",highRiskPopupController);

        highRiskPopupController.$inject = ["$state"];

        function highRiskPopupController($state){
            
            var vm = this;
            
            vm.close = function(){
                vm.packet = false;
            }
        }
})();