( function(){

	"use strict";

	angular
		.module('BiomarkWebForm')
		.config(['$stateProvider','$urlRouterProvider','$locationProvider', 
				function ($stateProvider, $urlRouterProvider, $locationProvider) {
			
			var environment = "%ENVIRONMENT%";
			if(environment =='development'){
				$locationProvider.html5Mode(false).hashPrefix('');
			}else{
				$locationProvider.html5Mode(true);
			}

    		$urlRouterProvider.otherwise('/');

			$stateProvider
    			.state("home",{
		            url:"/",
					template:"<covid></covid>",
				})
				.state("home.check-in",{
					url:"check-in/:barcode",
					template:"<check-in></check-in>",
				})
				.state("home.withdraw",{
					url:"withdraw",
					template:"<withdraw></withdraw>",
				})
				.state("home.symptom",{
					url:"symptom",
					template:"<symptom></symptom>",
				})
		}])
})();

