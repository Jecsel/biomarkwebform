( function(){

	"use strict";

	angular
		.module('BiomarkWebForm',[
			'ui.router',
			'ngStorage',
			'ngMaterial'
		])
		.filter("trust", ['$sce', function($sce) {
            return function(htmlCode){
              return $sce.trustAsHtml(htmlCode);
            }
		}])
		
})();