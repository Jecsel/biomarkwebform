( function(){
	//OEP
	"use strict";

	angular
		.module("BiomarkWebForm")
		.constant("BiomarkConfig",{
			host:"%HOST_API%",
			socket:"%SOCKET_API%",
			assets:"%PUBLIC_ASSETS%"
		})
})();
