(function(){
    "use strict";

    angular
        .module("BiomarkWebForm")
        .component("withdraw",{
            controller:"withdrawController",
            controllerAs:'vm',
            templateUrl:"/covid/withdraw/view.html"
        })
})();