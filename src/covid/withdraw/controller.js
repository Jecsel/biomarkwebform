(function(){
    "use strict";

    angular
        .module("BiomarkWebForm")
        .controller("withdrawController",withdrawController);

        withdrawController.$inject = ["webFormService","$state", "Http"];

        function withdrawController( webFormService, $state, Http){
            
            var vm = this;
            vm.data = {terms:false};
            vm.contact_number_invalid = true;
            vm.pop = false;
            vm.withdraw = {}
            vm.country_id = "MY";
            

            // vm.$onInit = function(){
            //     vm.booking = webFormService.get_booking_data();
            //     vm.booking.referral_code = $state.params.ref_code;
            //     webFormService.data = vm.booking;
            //     webFormService.save();
            // }

            vm.continue = function(){
                if(vm.validate()){
                    switch(vm.country_id){
                        case "MY":
                            vm.data.contactNumber = "+60"+vm.data.contact_number;
                            break;
                        case "ID":
                            vm.data.contactNumber = "+62"+vm.data.contact_number;
                            break;
                        case "SG":
                            vm.data.contactNumber = "+65"+vm.data.contact_number;
                            break;
                        case "PH":
                            vm.data.contactNumber = "+63"+vm.data.contact_number;
                            break;
                        default:
                            vm.data.contactNumber = "+60"+vm.data.contact_number;
                            break;
                    }

                    vm.withdraw = {
                        fullname: vm.data.whole_name,
                        contact_number: vm.data.contactNumber,
                        email_address: vm.data.email_address,
                        company_name: vm.data.company_name,
                        is_consent: vm.data.terms,
                        ic_number: vm.data.passport,
                        gender_id: vm.data.gender_id

                    }
                    
                    Http
                    .post("v1/webform/withdraw", vm.withdraw)
                    .then(function(res){
                    
                        vm.pop = true; 
                    });
                }
            }

            vm.validate = function(){

                if(!vm.data.whole_name){
                    return false;
                }
                if(!vm.data.contact_number){
                    return false;
                }
                if(!vm.data.company_name){
                    return false;
                }
                if(!vm.data.gender_id){
                    return false;
                }
                if(!vm.data.passport){
                    return false;
                }
                if(!vm.data.terms){
                    return false;
                }
                
                return true;
            }
        }
})();