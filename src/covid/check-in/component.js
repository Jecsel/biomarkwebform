(function(){
    "use strict";

    angular
        .module("BiomarkWebForm")
        .component("checkIn",{
            controller:"checkInController",
            controllerAs:"vm",
            templateUrl:"/covid/check-in/view.html"
        })
})();
