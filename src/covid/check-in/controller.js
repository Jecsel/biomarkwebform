(function(){
    "use strict";

    angular
        .module("BiomarkWebForm")
        .controller("checkInController",checkInController);

        checkInController.$inject = ["webFormService","$state","DateDropdown", "$stateParams", "Http", "$localStorage","$window"];

        function checkInController( webFormService, $state,DateDropdown, $stateParams, Http, $localStorage, $window){
            
            var vm = this;
            vm.data = {};
            vm.color = {};
            vm.pop = false;
            vm.contact_number_invalid = true;
            vm.barcode = $stateParams.barcode;
            vm.risk = {};

            vm.$onInit = function(){
                Http
                    .get("v1/webform/profiles/"+vm.barcode)
                    .then(function(res){
                        vm.active = true;
                        vm.profile = res.data;
                        vm.data.company_name = vm.profile.company.name;
                        vm.data.company_id = vm.profile.company.id;
                     },
                     function(res){
                        vm.active = false;
                        // TODO: redirect to invalid page
                     });
            }
            vm.continue = function(){
                if(vm.validate()){
                    switch(vm.country_id){
                        case "MY":
                            vm.data.contactNumber = "+60"+vm.data.contact_number;
                            break;
                        case "ID":
                            vm.data.contactNumber = "+62"+vm.data.contact_number;
                            break;
                        case "SG":
                            vm.data.contactNumber = "+65"+vm.data.contact_number;
                            break;
                        case "PH":
                            vm.data.contactNumber = "+63"+vm.data.contact_number;
                            break;
                        default:
                            vm.data.contactNumber = "+60"+vm.data.contact_number;
                            break;
                    }
                    
                    Http
                        .post("v1/webform/profiles", {
                            profile: {
                                fullname: vm.data.whole_name,
                                contact_number: vm.data.contactNumber,
                                gender_id: vm.data.gender_id,
                                ic_number: vm.data.passport,
                                company_id: vm.data.company_id,
                                barcode: vm.barcode,
                                barcode_type: vm.profile.barcode_type
                            }
                        })
                        .then(
                        function(res){
                            // vm.data.birth_date = new Date(vm.data.birth_date.year+"-"+(vm.data.birth_date.month+1)+"-"+vm.data.birth_date.day);
                          
                            Http.set_token(res.data.access_token);

                            Http
                            .get("v1/webform/symptoms")
                            .then(function(res){
                               
                                if (res.data.name == "No Questionnaire"){
                                    vm.color = {
                                        color: vm.style
                                    }
                    
                                    vm.risk = res.data;
                                    vm.pop = true; 
                                } else {
                                    $state.go("home.symptom");
                                }
                            });                           
                        },
                        function(err){
                            console.log(err)
                        });
                }
            }

            vm.validate = function(){

                if(!vm.data.whole_name){
                    return false;
                }
                if(!vm.data.contact_number){
                    return false;
                }
                if(!vm.data.company_name){
                    return false;
                }
                if(!vm.data.gender_id){
                    return false;
                }
                if(!vm.data.passport){
                    return false;
                }
                if(!vm.data.terms){
                    return false;
                }
                
                return true;
            }
        }
})();