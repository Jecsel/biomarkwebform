(function(){
    "use strict";

    angular
        .module("BiomarkWebForm")
        .component("symptom",{
            controller:"symptomController",
            controllerAs:"vm",
            templateUrl:"/covid/symptom/view.html"
        })
})();