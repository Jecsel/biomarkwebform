(function(){
    "use strict";

    angular
        .module("BiomarkWebForm")
        .service("RiskService",RiskService);

        RiskService.$inject = [];

        function RiskService(){
            var risk_response;

            this.set_risk = function(response) {
                risk_response = response;
            };

            this.get_risk = function(){
                return risk_response;
            };
        }
})();