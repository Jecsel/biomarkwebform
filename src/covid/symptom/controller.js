(function(){
  "use strict";

  angular
      .module("BiomarkWebForm")
      .controller("symptomController",symptomController);

      symptomController.$inject = ["webFormService","$state","Http", "RiskService"];

      function symptomController( webFormService, $state, Http, RiskService){
          var vm = this;
          vm.data = {};
          vm.pop = false;
          vm.result = [];
          vm.selectedRadio = "";
          vm.selectedCheckbox = {};
          vm.valid_symptoms = {
            temp_isValid : false,
            checkbox_isValid : false,
            form_isValid: true,
            enabledButton:false
          }
          vm.model = {};
          vm.color = {};
          vm.options = {};
          vm.cb_array = [];
          vm.risk = {};
          vm.$onInit = function(){
                Http
                  .get("v1/webform/symptoms")
                  .then(function(res){
                      vm.form_module = res.data;
                      vm.model = {
                        form_module_id: res.data.id,
                        barcode: res.data.barcode,
                        barcode_type: res.data.barcode_type,
                        form_content: []
                      }
                      angular.forEach(res.data.form_content, function(val, key){
                        if (val.is_question) {
                          vm.model.form_content.push({
                            form_content_id: val.id,
                            values: val.widget_content.default
                          });
                        }
                      })
                  });
                  if (vm.model == {}){
                      vm.validate();
                  }
                  
          }
          vm.continue = function(){  
            
            if (vm.model.form_content.length > 4){
              if (vm.model.form_content.length == 5){
                vm.model.form_content[4].values = parseFloat(vm.data.temperature).toFixed(1);
              }
            } 
            if (vm.model.form_content.length == 1){
              vm.model.form_content[0].values = parseFloat(vm.data.temperature).toFixed(1);
            }
            
            Http
              .post("v1/webform/form_answer", vm.model)
              .then(function(res){
                var widgetTag = res.data.form_content[0].widget_tag;
                  switch(widgetTag){
                  case "assessed_risk_green":
                  case "assessed_risk_no":
                      vm.style = "green";
                  break;
                  case "assessed_risk_red":
                      vm.style = "red";
                  break;
                  case "assessed_risk_yellow":
                      vm.style = "yellow";
                  break;
                }

                vm.color = {
                  color: vm.style
                }

                vm.risk = res.data;
                vm.pop = true; 
            });
            
          }

          vm.validate = function(indx, checbox_name){
            vm.valid_symptoms.form_isValid = true;
            vm.valid_symptoms.enabledButton = false;

            switch (vm.form_module.name) {
              case 'Symptoms & Temperature':
                validateCheckbox(indx, checbox_name);
                validateSymptom();
                validateTemperature();
                
                if(vm.valid_symptoms.temp_isValid && vm.valid_symptoms.checkbox_isValid && vm.valid_symptoms.form_isValid){
                  vm.valid_symptoms.enabledButton = true;
                }
                break;
              case 'Symptoms':
                validateCheckbox(indx, checbox_name);
                validateSymptom();

                if(vm.valid_symptoms.checkbox_isValid && vm.valid_symptoms.form_isValid){
                  vm.valid_symptoms.enabledButton = true;
                }
                break;
              case 'Temperature':
                validateTemperature();
                if(vm.valid_symptoms.temp_isValid){
                  vm.valid_symptoms.enabledButton = true;
                }
                break;

              default:
                break;
            }


          }

          function validateTemperature(){
            //Temperature Validation
            var tempInt = vm.data.temperature;
            
            if (tempInt != undefined) {
              if (tempInt >= 35 && tempInt <= 45) {
                vm.valid_symptoms.temp_isValid = true;
                if(tempInt.toString().length > 4){
                  vm.valid_symptoms.temp_isValid = false;
                }
              } else {
                vm.valid_symptoms.temp_isValid = false;
              }
            } else {
              vm.valid_symptoms.temp_isValid = false;
            }
          }

        function validateSymptom() {
           //Form Validation
           for (var i = 0; i < vm.model.form_content.length; i++) {
            if (vm.model.form_content[i].values == '') {
              vm.valid_symptoms.form_isValid = false;
            }
          }
        }
        
        function validateCheckbox(indx, checbox_name) {
          //checkbox Validation
          var checked = document.querySelectorAll('input#checkmark:checked');
          if (checked.length != 0) {
            vm.valid_symptoms.checkbox_isValid = true;
          } else {
            vm.valid_symptoms.checkbox_isValid = false;
          }

          if (checbox_name != undefined) {
              if(checbox_name == 'None'){
                vm.cb_array = ["None"];
              }else{
                for( var i = 0; i < vm.cb_array.length; i++){ 
                  if(vm.cb_array[i] === "None"){
                    vm.cb_array.splice(i, 1);
                  }
                }
                if(!vm.cb_array.includes(checbox_name)){
                  vm.cb_array.push(checbox_name);
                }
              }

            // push boolean for validation
            if (checbox_name == "None" && objectLength(vm.options) != 0) {
              for (var i = 0; i < vm.form_module.form_content[3].widget_content.values.length; i++) {
                if (i == (vm.form_module.form_content[3].widget_content.values.length) - 1) {
                  vm.options[(vm.form_module.form_content[3].widget_content.values.length) - 1] = true;
                } else {
                  vm.options[i] = false;
                }
              }
            } else {
              var n = vm.form_module.form_content[3].widget_content.values.length;
              for (var i = 0; i < n; i++) {
                if (vm.form_module.form_content[3].widget_content.values[i] == "None") {
                  vm.options[i] = false;
                }
              }
            }
            vm.model.form_content[3].values = vm.cb_array.toString();
          }
          
        }

          function objectLength(obj) {
            var result = 0;
            for(var prop in obj) {
              if (obj.hasOwnProperty(prop)) {
                result++;
              }
            }
            return result;
          }
      }
})();