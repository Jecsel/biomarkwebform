(function(){
    "use strict";

    angular
        .module("BiomarkWebForm")
        .component("covid",{
            controller:"webFormController",
            templateUrl:"/covid/view.html"
        })
})();