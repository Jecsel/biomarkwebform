(function(){
    "use strict";

    angular
        .module("BiomarkWebForm")
        .factory("webFormService",webFormService);

        webFormService.$inject = ["$localStorage"];

        function webFormService( $localStorage ){
            var f = {};
            f.data = {};
            f.clear = function(){
                $localStorage.booking = {};
            }
            f.save =  function(){
                $localStorage.booking = f.data;
            }
            f.get_booking_data = function(){
                return $localStorage.booking || {};
            }
            return f;
        }
})();