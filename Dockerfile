FROM node:10.9.0
RUN npm install -g gulp-cli
RUN npm install -g bower

RUN mkdir biomark-doctor-portal
ADD . /biomark-doctor-portal
WORKDIR /biomark-doctor-portal

RUN npm install 
RUN bower install --allow-root

