/***
* BIOMARK TECHNOLOGIES
***/
const gulp              = require('gulp');
const compiler          = require('./lib/compiler');
const browserSync       = require('browser-sync').create();

/** task registration */
gulp.task('clean',compiler.clean);
gulp.task('compile_js',compiler.compile_js);
gulp.task('compile_sass',compiler.compile_sass);
gulp.task('compile_views',compiler.compile_views);
gulp.task('copy_images',compiler.copy_images);
gulp.task('compile_vendors_js',compiler.compile_vendors_js);
gulp.task('compile_vendors_css',compiler.compile_vendors_css);
gulp.task('apply_configurations',compiler.apply_configurations);
/** production helper */
gulp.task('compress_js_scripts',compiler.compress_js_scripts);
gulp.task('compress_css_scripts',compiler.compress_css_scripts);
gulp.task('inject_compiled_assets',compiler.inject_compiled_assets);
gulp.task('upload_to_s3',compiler.upload_to_s3);
gulp.task('invalidate_cache',compiler.invalidate_cache);

gulp.task('init',compiler.init);

gulp.task('serve',function() {
	browserSync.init({
        server: "release/development/",
        ghostMode   : false
	});
});
gulp.task('default', gulp.parallel('serve'));
gulp.task('compile', 
    gulp.series([
        'init',
        'clean',
        gulp.parallel(['compile_js','compile_sass','compile_views','copy_images','compile_vendors_js','compile_vendors_css']),
        'apply_configurations'
    ])
);
gulp.task('deploy', 
    gulp.series([
        'init',
        'clean',
        gulp.parallel(['compile_js','compile_sass','compile_views','copy_images','compile_vendors_js','compile_vendors_css']),
        'apply_configurations',
        // gulp.parallel(['compress_css_scripts','compress_js_scripts']),
        // 'inject_compiled_assets',
        'upload_to_s3',
        'invalidate_cache'
    ])
);

