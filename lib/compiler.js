const gulp          = require('gulp');
const config        = require("./configurations");
const sass          = require('gulp-sass');
const concat        = require('gulp-concat');
const cleanCSS      = require('gulp-clean-css');
const templateCache = require('gulp-angular-templatecache');
const clean         = require('gulp-clean');
const replace       = require('gulp-replace');
const pump          = require('pump');
const inject        = require('gulp-inject');
const cloudfront    = require('gulp-cloudfront-invalidate');
var babel           = require('gulp-babel');
const dependecies   = require('wiredep')({
    directory: "./bower_components"
});

const module_name   =  config.MODULE_NAME;
const source_code   =  config.SOURCE;
const development   =  config.DEVELOPMENT;
const production    =  config.PRODUCTION;
const version       = Math.round((new Date()).getTime() / 1000);
const compiler = {
    init:function( cb ){
        if(!process.env.CI_COMMIT_REF_NAME) process.env.CI_COMMIT_REF_NAME = 'dev';
        cb();
    },
    invalidate_cache: function( cb ){
        var settings = {
            distribution: config.STAGING[process.env.CI_COMMIT_REF_NAME].cloudfront_distribution_id, // Cloudfront distribution ID
            paths: ['/*'] ,        
            accessKeyId: process.env.accessKeyId,            
            secretAccessKey: process.env.secretAccessKey,        
        }
        gulp
            .src('*')
            .pipe(cloudfront(settings))
            cb();
    },
    upload_to_s3: function( cb ){
        console.log("uploading to ",config.STAGING[process.env.CI_COMMIT_REF_NAME] );
        var s3   = require('gulp-s3')
        var AWS = {
            "key":    process.env.accessKeyId, 
            "secret": process.env.secretAccessKey, 
            "bucket": config.STAGING[process.env.CI_COMMIT_REF_NAME].bucket,
            "region": "ap-southeast-1"
        }
        // gulp.src(config.PRODUCTION+"**").pipe(s3(AWS)).on('end', cb )
        gulp.src(config.DEVELOPMENT+"**").pipe(s3(AWS)).on('end', cb )
    },
    inject_compiled_assets: function( cb ){
        gulp
            .src(production+'index.html')
            .pipe(inject(gulp.src([
                production+'style.'+version+'.css',
                production+'script.'+version+'.js',
                ],{read: false}),{relative: true,addPrefix:config.STAGING[process.env.CI_COMMIT_REF_NAME].url}))
            .pipe(gulp.dest(production)).on('end', cb )
    },
    compress_css_scripts: function(cb){
        console.log("TASK: Compressing CSS");
        pump([
                gulp
                    .src([ development+'css/vendor.css',development+'css/style.css'])
                    .pipe( concat('style.'+version+'.css') )
                    .pipe(cleanCSS({compatibility: 'ie8'})),
                gulp
                    .dest(production)
                    .on('end',cb)
            ]
        );
    },
    compress_js_scripts: function( cb ){
        console.log("TASK: Compressing & Uglifying JS");
        pump([
            gulp
                .src([ development+'js/vendor.js',development+'js/script.js',development+'js/cache.js'])
                .pipe( concat('script.'+version+'.js') )
                // .pipe(babel({presets: ['es2015']}))
                ,
            gulp
                .dest(production)
                .on('end', cb )
            ]
        );
    },
    apply_configurations: function( cb ){
        gulp
            .src([development+'js/script.js'])
            .pipe(replace('%HOST_API%',config.STAGING[process.env.CI_COMMIT_REF_NAME ].api))
            .pipe(replace('%ENVIRONMENT%',config.STAGING[process.env.CI_COMMIT_REF_NAME ].env))
            .pipe(replace('%PUBLIC_ASSETS%',config.STAGING[process.env.CI_COMMIT_REF_NAME ].public_assets))
            .pipe(gulp.dest(development+'js/')).on('end',function(){
                cb();
            })
    },
    compile_vendors_js: function( cb ){
        console.log("TASK: Building Depedencies JS");
        gulp
            .src(dependecies.js)
            .pipe( concat('vendor.js') )
            .pipe(gulp.dest(development+'js/'))
            .on('end',cb)
        
    },
    compile_vendors_css: function( cb ){
        console.log("TASK: Building Depedencies CSS");
        gulp
            .src(dependecies.css)
            .pipe( concat('vendor.css') )
            .pipe(gulp.dest(development+'css/')).on('end',cb)
            .on('end',cb)
    },
    clean: function(cb){
        console.log("TASK: Deleting compiled assets");
        gulp
            .src([
                    development+"css",
                    development+"js",
                    development+"images",
                    production+"*.js",
                    production+"*.css"
                ], 
                {allowEmpty: true}
            )
            .pipe(clean())
            setTimeout(cb,200);
        // cb();
    },
    compile_js: function(cb){
        console.log("TASK: Compiling JS");
        gulp
            .src( source_code+"**/*.js")
            .pipe( concat('script.js') )
            .pipe(gulp.dest(development+'js/'))
            .on('end',cb)
    },
    copy_images: function( cb ){
        console.log("TASK: Copying IMAGES");
        gulp
            .src(source_code+"images/**/*.*")
            .pipe(gulp.dest(development+'images/'))
            .on('end',cb)
    },
    compile_sass: function( cb){
        console.log("TASK: Compiling SASS");
        gulp
            .src( source_code+"**/*.scss")
            .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
            .pipe( concat('style.css') )
            .pipe(cleanCSS({compatibility: 'ie8'}))
            .pipe(gulp.dest(development+'css'))
            .on('end',cb)
    },
    
    compile_views: function( cb ){
        console.log("TASK: Compiling VIEWS");
        gulp
            .src(source_code+"**/*.html")
            .pipe(templateCache("cache.js",{module: module_name}))
            .pipe(gulp.dest(development+'js'))
            .on('end',cb);
    }
};

module.exports = compiler;