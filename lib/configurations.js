/**
 * GLOBAL CONFIGURATION SYSTEM
 * all configuration will reflect to app itself
 */
module.exports = {
    MODULE_NAME:"BiomarkWebForm",
    SOURCE:"./src/",
    DEVELOPMENT:"./release/development/",
    PRODUCTION:"./release/production/",
    STAGING:{
        INTERNAL:{
            name:"Webform Internal",
            url:"https://internal-covid.biomarking.com",
            bucket:"internal-covid.biomarking.com",
            api:"https://internal-service.biomarking.com/",
            acl:"public-read",
            cloudfront_distribution_id:"E2ZLB7P4G38SXO",
            public_assets:"//assets.biomarking.com/",
            env:"development"
        },
        master:{
            name:"Webform Production",
            url:"https://covid.biomarking.com",
            bucket:"covid.biomarking.com",
            api:"https://api.biomarking.com/",
            acl:"public-read",
            cloudfront_distribution_id:"E18V9I1THC40SH",
            public_assets:"//assets.biomarking.com/",
            env:"development"
        },
        dev:{
            // api:"https://demo-service.biomarking.com/",
            // socket:"wss://demo-service.biomarking.com/cable",
            // public_assets:"//assets.biomarking.com/",
            // api:"http://localhost:4000/",
            // socket:"wss://localhost:4000/cable",
            api:"https://internal-service.biomarking.com/",
            // api:"https://demo-service.biomarking.com/",
            socket:"wss://internal-service.biomarking.com/cable",
            env:"development"
        },
        
    }
};
