# BiomarkWebForm


## Installation
```ruby
	npm install
	bower install
	gulp	
```

## development

```ruby
	gulp build
```

#change log

```ruby
	v 1.0.1
	-profile view
	-profile update
	
	v 1.0.3
	-upload profile

	v 1.0.4
	-qr code view my profile
	-fix when error ignored at contact form
	v 1.0.5
	-patient connect
	-doctors notification
	-fixes
```

code for orders and notifications
```html
<li class="nav-item"> 
				<div class="dropdown">
					<a class="nav-link"> 
						<i class="header-icon fa fa-shopping-cart"></i> 
						<label>0</label> 
					</a>
					
					<div class="dropdown-content cart">
						<div class="dropdown-carret user-cart">
					        <div class="arrow-up"></div>
					    </div>
						<div class="user_dropdown_container" style="height: 70vh; overflow-y: auto">
							<p class="dropdown-title">Your Cart</p>
							
							
							<!-- <div class="dp-cart-item-wrapper" ng-repeat="a in [1,2]">
								<div class="cart-user-icon">
									
								</div>
								<div class="cart-user-detail">
									<p class="cart-user-name">Connie Smith</p>
									<div class="product-item">
										<p class="product-name">Lipid Test</p>
										<p class="amount">$ 10.00</p>
										<div class="clearfix"></div>
									</div>
									<div class="product-item">
										<p class="product-name">Electrolytes Test</p>
										<p class="amount">$ 10.00</p>
										<div class="clearfix"></div>
									</div>

								</div>
								<div class="clearfix"></div>
								<br>
								<div class="border-bottom"></div>
							</div> -->
							<div class="voucher_wrapper">
								<p class="voucher_label">Enter your voucher code</p>
								<input type="text" class="form-control bio-custom-input">
								<div class="border-bottom"></div>
							</div>
							<div class="total_wrapper">
								<p class="left_label">Sub Total</p><p class="right_label">$ 0.00</p>
								<div class="clearfix"></div>
								<p class="left_label">Discount</p><p class="right_label">$ 0.00</p>
								<div class="clearfix"></div>
								<div class="border-bottom"></div>
								<br>
								<p class="left_label_total">Total</p><p class="right_label_total">$ 00.00</p>
								<div class="clearfix"></div>
							</div>
							<div class="action-button">
								<button class="btn btn-danger biomark-btn-primary" ng-click="$ctrl.send_invitation()">
                                Complete Order
                            </button>
							</div>
						</div>
					</div>
				</div>
			</li> 
			
			<li class="nav-item"> 
				<div class="dropdown">
					<a class="nav-link"> 
						<i class="header-icon fa fa-bell"></i> 
						<label>0</label> 
					</a>
					
					<div class="dropdown-content notif">
						<div class="dropdown-carret user-notif">
					        <div class="arrow-up"></div>
					    </div>
						<div class="user_dropdown_container">
								<p class="dropdown-title">Notifications</p>
								<!-- <div class="dp-notification_wrapper" ng-repeat="a in [1,2,3]">
									<div class="notif-icon">
										
									</div>
									<div class="notif-title">
										<p>New tests are available</p>
										<small>Today  ·  6:30 PM</small>
									</div>
									<div class="clearfix"></div>
									<div class="border-bottom"></div>
								</div> -->
								<p>No Notification</p>
								<div class="dp-notification-wrapper">
									<button class="btn btn-outline-secondary biomark-btn-secondary" ui-sref="notifications">See All Notifications</button>	
								</div>
						</div>
						
						
					</div>
				</div>
			</li> 
```
